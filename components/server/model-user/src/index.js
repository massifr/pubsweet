module.exports = {
  ...require('./graphql'),
  modelName: 'User',
  model: require('./user'),
}
