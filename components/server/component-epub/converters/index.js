module.exports = {
  'wax-vivliostyle-ucp': require('./wax-vivliostyle-ucp'),
  'wax-paged-ucp': require('./wax-paged-ucp'),
  'wax-vivliostyle-default': require('./wax-vivliostyle-default'),
  'wax-paged-default': require('./wax-paged-default'),
}
