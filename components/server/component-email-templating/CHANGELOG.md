# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.17...@pubsweet/component-email-templating@0.1.18) (2019-06-28)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.16...@pubsweet/component-email-templating@0.1.17) (2019-06-24)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.15...@pubsweet/component-email-templating@0.1.16) (2019-06-21)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.14...@pubsweet/component-email-templating@0.1.15) (2019-06-13)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.13...@pubsweet/component-email-templating@0.1.14) (2019-06-12)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.12...@pubsweet/component-email-templating@0.1.13) (2019-05-27)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.11...@pubsweet/component-email-templating@0.1.12) (2019-04-25)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.10...@pubsweet/component-email-templating@0.1.11) (2019-04-18)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.9...@pubsweet/component-email-templating@0.1.10) (2019-04-09)


### Bug Fixes

* **email-tamplating:** use triple-stash in order not to escape HTML ([a1406d8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a1406d8))





## [0.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.8...@pubsweet/component-email-templating@0.1.9) (2019-03-06)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.7...@pubsweet/component-email-templating@0.1.8) (2019-03-05)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.6...@pubsweet/component-email-templating@0.1.7) (2019-02-19)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.5...@pubsweet/component-email-templating@0.1.6) (2019-02-19)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.4...@pubsweet/component-email-templating@0.1.5) (2019-02-01)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.3...@pubsweet/component-email-templating@0.1.4) (2019-01-16)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.2...@pubsweet/component-email-templating@0.1.3) (2019-01-14)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.1...@pubsweet/component-email-templating@0.1.2) (2019-01-13)

**Note:** Version bump only for package @pubsweet/component-email-templating





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-email-templating@0.1.0...@pubsweet/component-email-templating@0.1.1) (2019-01-09)

**Note:** Version bump only for package @pubsweet/component-email-templating





# 0.1.0 (2018-12-12)


### Features

* add email templating component ([4baa6e0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4baa6e0))
