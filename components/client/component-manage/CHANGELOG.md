# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.7...pubsweet-component-manage@1.0.8) (2019-06-28)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.6...pubsweet-component-manage@1.0.7) (2019-06-24)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.5...pubsweet-component-manage@1.0.6) (2019-06-21)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.4...pubsweet-component-manage@1.0.5) (2019-06-13)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.3...pubsweet-component-manage@1.0.4) (2019-06-12)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.2...pubsweet-component-manage@1.0.3) (2019-05-27)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.1...pubsweet-component-manage@1.0.2) (2019-04-25)

**Note:** Version bump only for package pubsweet-component-manage





## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@1.0.0...pubsweet-component-manage@1.0.1) (2019-04-18)

**Note:** Version bump only for package pubsweet-component-manage





<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@0.2.4...pubsweet-component-manage@1.0.0) (2018-07-02)


### Bug Fixes

* **components:** import Manage styles in component ([6f0e443](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6f0e443))


### BREAKING CHANGES

* **components:** Manage.scss is now required in the component itself. Since this was usually
required from the app itself, this would now result in double importing of styles.




<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@0.2.3...pubsweet-component-manage@0.2.4) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-manage

<a name="0.2.3"></a>

## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@0.2.2...pubsweet-component-manage@0.2.3) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-manage
