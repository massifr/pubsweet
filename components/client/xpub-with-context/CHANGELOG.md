# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.22](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.21...xpub-with-context@0.1.22) (2019-06-28)

**Note:** Version bump only for package xpub-with-context





## [0.1.21](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.20...xpub-with-context@0.1.21) (2019-06-24)

**Note:** Version bump only for package xpub-with-context





## [0.1.20](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.19...xpub-with-context@0.1.20) (2019-06-21)

**Note:** Version bump only for package xpub-with-context





## [0.1.19](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.18...xpub-with-context@0.1.19) (2019-06-13)

**Note:** Version bump only for package xpub-with-context





## [0.1.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.17...xpub-with-context@0.1.18) (2019-06-12)

**Note:** Version bump only for package xpub-with-context





## [0.1.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.16...xpub-with-context@0.1.17) (2019-05-27)

**Note:** Version bump only for package xpub-with-context





## [0.1.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.15...xpub-with-context@0.1.16) (2019-04-25)

**Note:** Version bump only for package xpub-with-context





## [0.1.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.14...xpub-with-context@0.1.15) (2019-04-18)

**Note:** Version bump only for package xpub-with-context





## [0.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.13...xpub-with-context@0.1.14) (2019-04-09)

**Note:** Version bump only for package xpub-with-context





## [0.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.12...xpub-with-context@0.1.13) (2019-03-06)

**Note:** Version bump only for package xpub-with-context





## [0.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.11...xpub-with-context@0.1.12) (2019-03-05)

**Note:** Version bump only for package xpub-with-context





## [0.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.10...xpub-with-context@0.1.11) (2019-02-19)

**Note:** Version bump only for package xpub-with-context





## [0.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.9...xpub-with-context@0.1.10) (2019-02-19)

**Note:** Version bump only for package xpub-with-context





## [0.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.8...xpub-with-context@0.1.9) (2019-02-01)

**Note:** Version bump only for package xpub-with-context





## [0.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.7...xpub-with-context@0.1.8) (2019-01-16)

**Note:** Version bump only for package xpub-with-context





## [0.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.6...xpub-with-context@0.1.7) (2019-01-14)

**Note:** Version bump only for package xpub-with-context





## [0.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.5...xpub-with-context@0.1.6) (2019-01-13)

**Note:** Version bump only for package xpub-with-context





## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.4...xpub-with-context@0.1.5) (2019-01-09)

**Note:** Version bump only for package xpub-with-context





## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.3...xpub-with-context@0.1.4) (2018-12-12)

**Note:** Version bump only for package xpub-with-context





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.2...xpub-with-context@0.1.3) (2018-12-04)

**Note:** Version bump only for package xpub-with-context





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.1...xpub-with-context@0.1.2) (2018-11-30)

**Note:** Version bump only for package xpub-with-context





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.0...xpub-with-context@0.1.1) (2018-11-29)

**Note:** Version bump only for package xpub-with-context





<a name="0.1.0"></a>
# 0.1.0 (2018-11-05)


### Features

* GraphQL Xpub submit component ([ba07060](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba07060))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
