# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.12...xpub-upload@1.0.13) (2019-06-28)

**Note:** Version bump only for package xpub-upload





## [1.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.11...xpub-upload@1.0.12) (2019-06-24)

**Note:** Version bump only for package xpub-upload





## [1.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.10...xpub-upload@1.0.11) (2019-06-21)

**Note:** Version bump only for package xpub-upload





## [1.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.9...xpub-upload@1.0.10) (2019-06-13)

**Note:** Version bump only for package xpub-upload





## [1.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.8...xpub-upload@1.0.9) (2019-06-12)

**Note:** Version bump only for package xpub-upload





## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.7...xpub-upload@1.0.8) (2019-05-27)

**Note:** Version bump only for package xpub-upload





## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.6...xpub-upload@1.0.7) (2019-04-25)

**Note:** Version bump only for package xpub-upload





## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.5...xpub-upload@1.0.6) (2019-04-18)

**Note:** Version bump only for package xpub-upload





## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.4...xpub-upload@1.0.5) (2019-04-09)

**Note:** Version bump only for package xpub-upload





## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.3...xpub-upload@1.0.4) (2019-03-06)

**Note:** Version bump only for package xpub-upload





## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.2...xpub-upload@1.0.3) (2019-03-05)

**Note:** Version bump only for package xpub-upload





## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.1...xpub-upload@1.0.2) (2019-02-19)

**Note:** Version bump only for package xpub-upload





## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@1.0.0...xpub-upload@1.0.1) (2019-02-19)

**Note:** Version bump only for package xpub-upload





# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@0.0.7...xpub-upload@1.0.0) (2019-02-01)


### Code Refactoring

* temporarily remove unmigrated components ([32db6ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32db6ad))


### BREAKING CHANGES

* A lot of unmigrated (not yet moved from REST/Redux to GraphQL/Apollo system) bits
have changed. There might be some breaking changes as a result. This is a big migration involving
big changes - if you encounter anything weird, please contact us on GitLab or on Mattermost.





<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@0.0.6...xpub-upload@0.0.7) (2018-04-24)




**Note:** Version bump only for package xpub-upload

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@0.0.5...xpub-upload@0.0.6) (2018-04-03)




**Note:** Version bump only for package xpub-upload

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@0.0.4...xpub-upload@0.0.5) (2018-03-27)




**Note:** Version bump only for package xpub-upload

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-upload@0.0.3...xpub-upload@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-upload

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-upload
