# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-people-picker@0.1.4...@pubsweet/component-people-picker@0.1.5) (2019-06-28)

**Note:** Version bump only for package @pubsweet/component-people-picker





## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-people-picker@0.1.3...@pubsweet/component-people-picker@0.1.4) (2019-06-24)

**Note:** Version bump only for package @pubsweet/component-people-picker





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-people-picker@0.1.2...@pubsweet/component-people-picker@0.1.3) (2019-06-21)

**Note:** Version bump only for package @pubsweet/component-people-picker





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-people-picker@0.1.1...@pubsweet/component-people-picker@0.1.2) (2019-06-13)

**Note:** Version bump only for package @pubsweet/component-people-picker





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-people-picker@0.1.0...@pubsweet/component-people-picker@0.1.1) (2019-06-12)

**Note:** Version bump only for package @pubsweet/component-people-picker





# 0.1.0 (2019-05-27)


### Bug Fixes

* **peoplepicker:** added customisable people prop and filtering function ([03591a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/03591a0))
* fix PeoplePicker tests ([5691a66](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5691a66))
* **peoplepicker:** fixed icon import ([c9cdb8e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c9cdb8e))
* bottom button alignment ([0fb4ffd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0fb4ffd))
* changed selected item so that click event is triggered only by icon ([ad242ce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ad242ce))
* fix default theme styling of Searchbox ([34e0426](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/34e0426))
* fix styling for modal buttons ([1d34c5d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d34c5d))
* fix tests for SearchBox component ([99fe553](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/99fe553))
* fixed removal of people via suggested item ([7c573a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c573a0))
* **peoplepicker:** remove dead icon overriding code ([54753f8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54753f8))
* **peoplepicker:** remove the icon override ([05c5c33](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/05c5c33))
* put back the props spread of RemoveButton in SelectedItem ([8f81e34](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8f81e34))
* re-added custom Icon component so that we can use override ([06dc4b5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/06dc4b5))
* remove use of space variable in peoplepicker styling ([53354df](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53354df))
* removed rebass from MainColumn, added breakpoints to default theme ([c71920c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c71920c))
* tests for PeoplePickerLogic ([a78980c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a78980c))
* validation positioning ([f46bcf4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f46bcf4))


### Features

* **peoplepicker:** add onkeydown event handler to the search box ([361ba58](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/361ba58))
* **peoplepicker:** improve the placeholder of the search box ([e64d3b1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e64d3b1))
* **peoplepicker:** removing tests of unused clickability ([2833379](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2833379))
* **peoplepicker:** replace autosuggest with textfield for people picker ([0015456](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0015456))
* **peoplepicker:** user props for the placeholder of the search box ([bd0a4dd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bd0a4dd))
* allow override of input component in searchbox (broken) ([59d76e8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/59d76e8))
* **peoplepicker:** wip ([5564271](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5564271))
* add some docs about the input override and make it work ([fd1b9ac](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fd1b9ac))
* fix alignment of search box and person pod button ([3e5659e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3e5659e))
* merge commit, fix styling on icons and fonts ([33e5e80](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/33e5e80))
* move media query mixin to ui-toolkit ([1bee71a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1bee71a))
* re-add the modal using new pubsweet version ([fdc7101](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fdc7101))
* removed ChooserPod ([c8d4333](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c8d4333))
* started decoupling from rebass ([99d683a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/99d683a))
* worked on PersonPod styling ([ade2338](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ade2338))
