# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.14...xpub-theme@0.0.15) (2019-06-28)

**Note:** Version bump only for package xpub-theme





## [0.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.13...xpub-theme@0.0.14) (2019-06-24)

**Note:** Version bump only for package xpub-theme





## [0.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.12...xpub-theme@0.0.13) (2019-06-21)

**Note:** Version bump only for package xpub-theme





## [0.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.11...xpub-theme@0.0.12) (2019-06-13)

**Note:** Version bump only for package xpub-theme





## [0.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.10...xpub-theme@0.0.11) (2019-06-12)

**Note:** Version bump only for package xpub-theme





## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.9...xpub-theme@0.0.10) (2019-05-27)

**Note:** Version bump only for package xpub-theme





## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.8...xpub-theme@0.0.9) (2019-04-25)

**Note:** Version bump only for package xpub-theme





## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.7...xpub-theme@0.0.8) (2019-04-18)

**Note:** Version bump only for package xpub-theme





<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.6...xpub-theme@0.0.7) (2018-05-03)




**Note:** Version bump only for package xpub-theme

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.5...xpub-theme@0.0.6) (2018-04-03)




**Note:** Version bump only for package xpub-theme

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.4...xpub-theme@0.0.5) (2018-03-27)




**Note:** Version bump only for package xpub-theme

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-theme@0.0.3...xpub-theme@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-theme

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-theme
