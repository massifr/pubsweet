# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.13...pubsweet-component-epub-frontend@0.1.14) (2019-06-28)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.12...pubsweet-component-epub-frontend@0.1.13) (2019-06-24)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.11...pubsweet-component-epub-frontend@0.1.12) (2019-06-21)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.10...pubsweet-component-epub-frontend@0.1.11) (2019-06-13)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.9...pubsweet-component-epub-frontend@0.1.10) (2019-06-12)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.8...pubsweet-component-epub-frontend@0.1.9) (2019-05-27)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.7...pubsweet-component-epub-frontend@0.1.8) (2019-04-25)

**Note:** Version bump only for package pubsweet-component-epub-frontend





## [0.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.6...pubsweet-component-epub-frontend@0.1.7) (2019-04-18)

**Note:** Version bump only for package pubsweet-component-epub-frontend





<a name="0.1.6"></a>
## [0.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.5...pubsweet-component-epub-frontend@0.1.6) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-epub-frontend

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.4...pubsweet-component-epub-frontend@0.1.5) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-epub-frontend

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.3...pubsweet-component-epub-frontend@0.1.4) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-epub-frontend

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.2...pubsweet-component-epub-frontend@0.1.3) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-epub-frontend
