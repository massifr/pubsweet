# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.12...@pubsweet/models@0.2.13) (2019-06-28)

**Note:** Version bump only for package @pubsweet/models





## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.11...@pubsweet/models@0.2.12) (2019-06-24)

**Note:** Version bump only for package @pubsweet/models





## [0.2.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.10...@pubsweet/models@0.2.11) (2019-06-21)

**Note:** Version bump only for package @pubsweet/models





## [0.2.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.9...@pubsweet/models@0.2.10) (2019-06-13)

**Note:** Version bump only for package @pubsweet/models





## [0.2.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.8...@pubsweet/models@0.2.9) (2019-06-12)

**Note:** Version bump only for package @pubsweet/models





## [0.2.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.7...@pubsweet/models@0.2.8) (2019-05-27)

**Note:** Version bump only for package @pubsweet/models





## [0.2.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.6...@pubsweet/models@0.2.7) (2019-04-25)

**Note:** Version bump only for package @pubsweet/models





## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.5...@pubsweet/models@0.2.6) (2019-04-18)

**Note:** Version bump only for package @pubsweet/models





## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.4...@pubsweet/models@0.2.5) (2019-04-09)


### Bug Fixes

* remove some circular dependencies ([1c0cf11](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1c0cf11))
* safeguard require ([1aab6ab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1aab6ab))





## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.3...@pubsweet/models@0.2.4) (2019-03-06)

**Note:** Version bump only for package @pubsweet/models





## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.2...@pubsweet/models@0.2.3) (2019-03-05)

**Note:** Version bump only for package @pubsweet/models





## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.1...@pubsweet/models@0.2.2) (2019-02-19)

**Note:** Version bump only for package @pubsweet/models





## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.0...@pubsweet/models@0.2.1) (2019-02-19)

**Note:** Version bump only for package @pubsweet/models





# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.3...@pubsweet/models@0.2.0) (2019-02-01)


### Features

* **models:** support for multiple models in a single component ([caed5be](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/caed5be))





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.2...@pubsweet/models@0.1.3) (2019-01-16)

**Note:** Version bump only for package @pubsweet/models





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.1...@pubsweet/models@0.1.2) (2019-01-14)

**Note:** Version bump only for package @pubsweet/models





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.0...@pubsweet/models@0.1.1) (2019-01-13)

**Note:** Version bump only for package @pubsweet/models





# 0.1.0 (2019-01-09)


### Features

* introduce [@pubsweet](https://gitlab.coko.foundation/pubsweet)/models package ([7c1a364](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c1a364))
