variables:
  IMAGE_ORG: pubsweet
  IMAGE_NAME: pubsweet
  BASE_DOMAIN: gateway.ps.elifesciences.yld.io
  CONFIGURATION_REPOSITORY: https://gitlab.coko.foundation/pubsweet/deployment-config.git

stages:
  - build
  - test
  - review
  - audit
  - staging
  - production

build:
  image: docker:latest
  stage: build
  except:
    - tags
  script:
    - docker version
    - docker build -t $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA .
    - if [ -z "$DOCKERHUB_USERNAME" ] || [ -z "$DOCKERHUB_PASSWORD" ]; then echo "Not pushing" && exit 0; fi
    - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_PASSWORD
    - echo "Ignore warning! Cannot perform an interactive login from a non TTY device"
    - docker push $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA

pages:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: production
  script:
    - cd ${HOME}
    - yarn styleguide:build
    - cp -R docs/styleguide/ /builds/pubsweet/pubsweet/public/
  artifacts:
    paths:
      - public/
  only:
    - master # this job will affect only the 'master' branch

pages:review:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: review
  script:
    - cd ${HOME}
    - yarn styleguide:build
    - cp -R docs/styleguide/ /builds/pubsweet/pubsweet/review/
  artifacts:
    paths:
      - review/
  except:
  - master
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_PROJECT_NAMESPACE.coko.foundation/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/review/index.html

audit:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: audit
  allow_failure: true
  except:
    - tags
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - yarn audit

lint:style:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: test
  except:
    - tags
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - yarn lint:style

lint:js:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: test
  except:
    - tags
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - yarn lint:js

lint:json:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: test
  except:
    - tags
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - yarn lint:json

lint:commits:
   image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
   stage: test
   except:
     - tags
   script:
     - cp -r .git ${HOME}/ && cd ${HOME}
     - yarn commitlint --from=origin/master --to=$CI_COMMIT_SHA

test:
  image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
  stage: test
  variables:
    # don't clone repo as image already has it
    GIT_STRATEGY: none
    # setup data for postgres image
    POSTGRES_USER: test
    POSTGRES_PASSWORD: pw
    # connection details for tests
    PGUSER: test
    PGPASSWORD: pw
    NODE_ENV: test
  services:
    - postgres
  except:
    - tags
  script:
    - cd ${HOME}
    # specify host here else it confuses the linked postgres image
    - PGHOST=postgres yarn test

# if tests pass we will push latest, labelled with current commit hash
push:latest:
  image: docker:latest
  stage: staging
  script:
    - if [ -z "$DOCKERHUB_USERNAME" ] || [ -z "$DOCKERHUB_PASSWORD" ]; then echo "Not pushing" && exit 0; fi
    - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_PASSWORD
    - echo "Ignore warning! Cannot perform an interactive login from a non TTY device"
    - docker build -t $IMAGE_ORG/$IMAGE_NAME:latest --label COMMIT_SHA=$CI_COMMIT_SHA .
    - docker push $IMAGE_ORG/$IMAGE_NAME:latest
  only:
    - master
  except:
    - tags